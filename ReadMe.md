# Address Autocomplete script
svirepunchik@gmail.com (c) 2020

## Описание
Скрипт использует [API автодополнения адресов DaData](https://dadata.ru/api/suggest/address/) для автодополнения адресов.
Максимальное количество запросов - до 10 000 в сутки, поэтому в скрипте предусмотрено запрет отправки запросов, если:
* введено менее трёх символов;
* изменений в поле ввода адреса не было;
* с момента последнего запроса прошло  менее _500 мс_.

**Внимание**: _спиннер (лоадер) использует `FontAwesome`, предполагается, что он у Вас уже подключен._

## Установка для Bitrix
1. Разместить в папке `/bitrix/` следующее:  
    1.1. папку `css` поместить в `/bitrix/css/ddac/`.  
    1.2. файл `ddac.js` поместить в `/bitrix/js/%CUSTOM_FOLDER_NAME%/ddac/`.  
2. Зарегистрироваться на [DaData](https://dadata.ru) и получить API-ключ.
3. Поместить API-ключ в `ddac.js`.
4. В шаблоне компонента до вывода формы, содержащей поле ввода адреса с автодополнением необходимо подключить CSS:
    ```
    <? $this->addExternalCss("/bitrix/css/ddac/ddac.css");?>
    ```
5. В самой форме поместить рядом с полем ввода адреса:  
    5.1. HTML-элемент спиннера (лоадера):  
    ```
    <i id="ac_spinner" class="fab fa-superpowers"></i>
    ```  
    5.2. HTML-элемент списка автодополнения:  
    ```
    <select name="ac_list" class="autocomplete_list"></select>
    ```  
6. В шаблоне компонента после описания формы поместить инициализацию и вызов скрипта автодополнения:
    ```
    <script type="text/javascript">
        let ac_main = document.form_with_autocomplete; // ссылка на корневой HTML-элемент формы
        let ac_list = ac_main.ac_list; // ссылка на HTML-элемент SELECT формы, в котором будут появляться подсказки
        let ac_address_input = ac_main.ac_address; // ссылка на HTML-элемент INPUT формы, в котором будет происходить ввод адреса
        let ac_spinner = document.getElementById('ac_spinner'); // ссылка на HTML-элемент спиннера (лоадера). Появляется во время поиска автодополнений
    </script>
    <script type="text/javascript" src="/bitrix/js/%CUSTOM_FOLDER_NAME%/ddac/ddac.js"></script>
    ```  
## Установка для всего остального
1. Разместить файлы где-то в Вашей файловой системе.
2. Зарегистрироваться на [DaData.ru](https://dadata.ru) и получить API-ключ.
3. Поместить API-ключ в `ddac.js`.
4. Подключить `ddac.css` на странице с формой, содержащей поле с автодополнением адреса.
5. В самой форме поместить рядом с полем ввода адреса:  
    5.1. HTML-элемент спиннера (лоадера)  
    ```
    <i id="ac_spinner" class="fab fa-superpowers"></i>
    ```  
    5.2. HTML-элемент списка автодополнения  
    ```
    <select name="ac_list" class="autocomplete_list"></select>
    ```  
6. После описания формы поместить инициализацию вызов скрипта автодополнения:  
   ```
   <script type="text/javascript">
       let ac_main = document.form_with_autocomplete; // ссылка на корневой HTML-элемент формы
               let ac_list = ac_main.ac_list; // ссылка на HTML-элемент SELECT формы, в котором будут появляться подсказки
               let ac_address_input = ac_main.ac_address; // ссылка на HTML-элемент INPUT формы, в котором будет происходить ввод адреса
               let ac_spinner = document.getElementById('ac_spinner'); // ссылка на HTML-элемент спиннера (лоадера). Появляется во время поиска автодополнений
   </script>
   <script type="text/javascript" src="/%PATH_TO_DDAC_FOLDER%/ddac.js"></script>
   ```  
