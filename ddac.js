/* svirepunchik@gmail.com (c) 2020*/

// constants
const AC_URL = 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address'; // dadata.ru API endpoint
const AC_AUTH_TOKEN = '_YOUR_API_KEY_HERE_'; // datata.ru API key
const AC_OK_STATUS = 200; // HTTP ok code
const AC_DELTA = 500;  // time in milliseconds between requests
const AC_MIN_CHARS = 3; // minimum chars to enter before start autocomplete
const AC_VISIBLE = 'visible';
const AC_HIDDEN = 'hidden';
const AC_OPAQUE = '0';
const AC_NOTOPAUE = '1';

let ac_fragment = new DocumentFragment();
let ac_change_time;
let ac_previous_value;

function ac_xhrStateChange() {
    if(this.readyState === XMLHttpRequest.DONE && this.status === AC_OK_STATUS) {
        let response = JSON.parse(this.responseText);
        if (response.suggestions.length <= 0) return;
        for (let i = 0; i < ac_list.options.length; i++) {
            ac_list.options[i] = null;
        }
        response.suggestions.forEach(ac_addListItem);
        ac_list.append(ac_fragment);
        ac_list.style.visibility = AC_VISIBLE;
        ac_spinner.style.opacity = AC_OPAQUE;
    }
}

function ac_addListItem(item) {
    let option = document.createElement('option');
    option.innerHTML = item.value;
    option.value = item.value;
    ac_fragment.append(option);
}

function ac_onSelectChange(element) {
    ac_address_input.value = ac_list.options[ac_list.options.selectedIndex].value;
    ac_address_input.focus();
    ac_list.style.visibility = AC_HIDDEN;
}

function ac_getAutocomplete(query) {
    if (query === "") return;
    let request = {"query": query};
    let ac_xhr = new XMLHttpRequest();
    ac_xhr.open('POST', AC_URL, true);
    ac_xhr.setRequestHeader('Content-type', 'application/json');
    ac_xhr.setRequestHeader('Accept', 'application/json');
    ac_xhr.setRequestHeader('Authorization', 'Token ' + AC_AUTH_TOKEN);
    ac_xhr.onreadystatechange = ac_xhrStateChange;
    ac_spinner.style.opacity = AC_NOTOPAQUE;
    ac_xhr.send(JSON.stringify(request));
}

function ac_parse_keys(event) {
    let ret_val = true;
    switch (event.key) {
        case "ArrowUp":
        case "Up":
            event.preventDefault();
            if (ac_list.selectedIndex <= 0)
                ac_list.selectedIndex = (ac_list.options.length - 1);
            else ac_list.selectedIndex -= 1;
            ret_val = false;
            break;
        case "ArrowDown":
        case "Down":
            event.preventDefault();
            if (ac_list.selectedIndex >= (ac_list.options.length - 1))
                ac_list.selectedIndex = 0;
            else ac_list.selectedIndex += 1;
            ret_val = false;
            break;
        case "Enter":
            if (ac_list.style.visibility === AC_HIDDEN) {
                ret_val = true;
                break;
            }
            if (ac_list.options.length > 0
                && ac_list.options.selectedIndex >=0
                && ac_list.options.selectedIndex <= (ac_list.options.length - 1)
            ) {
                ac_address_input.value = ac_list.options[ac_list.options.selectedIndex].value;
                ac_list.style.visibility = AC_HIDDEN;
                ret_val = false;
            }
            break;
        case "Escape":
        case "Esc":
            ac_list.style.visibility = AC_HIDDEN;
            ret_val = false;
    }
    return ret_val;
}

function ac_onAutocompleteInputChange(event) {
    let do_request = ac_parse_keys(event);
    let curr_length = parseInt(ac_address_input.value.length);
    if (curr_length < AC_MIN_CHARS) return;
    if ((Date.now() - ac_change_time) < AC_DELTA) return;
    if (ac_previous_value === ac_address_input.value) return;
    ac_previous_value = ac_address_input.value;
    ac_change_time = Date.now();
    if (do_request) ac_getAutocomplete(ac_address_input.value);
}

function ac_onAutocompleteSubmit(event) {
    if (event.type === 'submit' && ac_list.style.visibility === AC_VISIBLE) {
	event.preventDefault();
	ac_onAutocompleteInputChange(event);
	return false;
    }
    if (event.type === 'submit') return true;
}

ac_main.addEventListener('submit', ac_onAutocompleteSubmit);
ac_address_input.addEventListener('keyup', ac_onAutocompleteInputChange);
ac_list.addEventListener('change', ac_onSelectChange);
